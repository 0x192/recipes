# Cafe Mexicali Sweet Pork

## Equipment

- Instant Pot
- Forks

## Ingredients

- 5 Pounds Boneless Pork Shoulder, Butt, or Boneless Pork Rib Meat
- 3 Tablespoons Menudo Spice Mix
- 1 Tablespoon Olive Oil
- 2 Cups Cold Water
- 1 Cup Brown Sugar
- 1 (16-ounce) Can Red Enchilada Sauce
- 1 (4-ounce) Can Chopped Green Chiles
- 12 ounces Coke

## Instructions

1. Place spice mix and pork into a large ziplock bag. Shake until pork is coated
2. In a pan, heat oil on high and sear pork
3. Place pork in instant pot and surround with water
4. Place lid on and seal instant pot. Set manual timer for 50 minutes
5. After 50 minutes, allow instant pot to naturally release pressure for about 40 minutes
6. Release remaining pressure and remove pork from the instant pot. Allow to cool for a few minutes
7. Shred meat with fork
8. Remove remaining juice from the pot and set aside in small bowl
9. Add enchilada sauce, chopped green chiles, brown sugar, and one can of soda to the instant pot and whisk together
10. Place shredded pork back into the instant pot and turn setting to ```saute```. Heat until sauce begins to boil and meat is heated through
11. Turn setting to ```keep warm```
12. (Optional) Add one tablespoon of cornstarch to liquids removed from the pot earlier and add back to the instant pot. Heat until thickened