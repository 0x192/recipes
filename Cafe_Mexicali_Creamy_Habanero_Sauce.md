# Cafe Mexicali Creamy Habanero Sauce (White Sauce)

## Equipment

- Large Non-Stick Pan
- Chef's Knife
- Whisk

## Ingredients

- 3 Tablespoons Butter
- 3 Tablespoons All Purpose Flour
- 1 Clove Garlic
- 2 Habanero Peppers
- 2 Teaspoons Sugar
- 2 Cups Heavy Cream
- 1 Cup Chicken Broth
- 1/2 Teaspoon Salt

## Instructions

1. Slice habaneros in half and remove the seeds
2. Add butter to a pan over medium heat
3. Once butter is melted, add your habaneros and minced garlic to the pan. Cook for about 2-3 minutes
4. Add flour to the pan. Cook for another 1-2 minutes 
5. Add heavy cream and sugar to the pan while whisking
6. Bring sauce to a boil, constantly stirring until thick
7. Reduce heat to medium and add in chicken broth. Whisk until combined
8. Continue to whisk until sauce reaches desired consistency